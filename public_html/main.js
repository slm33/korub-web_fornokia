function calculate(price1, value1, price2, value2) {
    var ratio1 = price1 / value1;
    var ratio2 = price2 / value2;
    var percent1 = ratio2 / ratio1 * 100 - 100;
    var percent2 = ratio1 / ratio2 * 100 - 100;
    percent1 = round(percent1, 2);
    percent2 = round(percent2, 2);

    var result1 = document.getElementById("result1");
    var result2 = document.getElementById("result2");

    if (ratio1 < ratio2) {
        result1.innerHTML = "Выгода " + percent1 + "%";
        result2.innerHTML = "";
    } else if (ratio1 > ratio2) {
        result1.innerHTML = "";
        result2.innerHTML = "Выгода " + percent2 + "%";
    } else {
        result1.innerHTML = "=";
        result2.innerHTML = "=";
    }
}

function check(input) {
    input.value = input.value.replace(/[ ]/g, '0');
    input.value = input.value.replace(/[aA]/g, '2');
    input.value = input.value.replace(/[dD]/g, '3');
    input.value = input.value.replace(/[gG]/g, '4');
    input.value = input.value.replace(/[jJ]/g, '5');
    input.value = input.value.replace(/[mM]/g, '6');
    input.value = input.value.replace(/[pP]/g, '7');
    input.value = input.value.replace(/[tT]/g, '8');
    input.value = input.value.replace(/[wW]/g, '9');
    input.value = input.value.replace(/[^0-9.]/g, '');

    var price1 = document.getElementById("price1").value;
    var price2 = document.getElementById("price2").value;
    var value1 = document.getElementById("value1").value;
    var value2 = document.getElementById("value2").value;

    if (price1 && price2 && value1 && value2 > 0) {
        calculate(price1, value1, price2, value2);
    }
}

/**
 * Округление числа
 * @param {number} value
 * @param {number} decimals - количество знаков после запятой
 * @returns {number}
 */
function round(value, decimals) {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}